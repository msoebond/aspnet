﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Cart
    {
        public int CartId { get; set; }
        private List<CartLine> lineCollection = new List<CartLine>();
        public IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public Product Product { get; set; }
        public int QtySets { get; set; }
    }
}
