﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Supplier
    {
        public int SupplierID { get; set; }
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
