﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class FakeRepository : IProductRepository
    {
        public IQueryable<Product> Products => new List<Product>{
            new Product{Name="Coke 12oz." },
            new Product{Name="Milk 1gal."}
            }.AsQueryable<Product>();

        public IQueryable<Supplier> Suppliers => throw new NotImplementedException();

        public IQueryable<Order> Orders => throw new NotImplementedException();

        public IQueryable<Cart> Carts => throw new NotImplementedException();
    }
}
