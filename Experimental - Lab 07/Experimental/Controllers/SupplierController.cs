﻿using Inventory.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inventory
{
    public class SupplierController : Controller
    {
        private ISupplierRepository repository;
        public SupplierController(ISupplierRepository repo)
        {
            repository = repo;
        }

        public ViewResult List() => View(repository.Suppliers);

        public IActionResult Index()
        {
            return View();
        }
    }
}