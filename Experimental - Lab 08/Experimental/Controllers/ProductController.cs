﻿using Inventory.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Inventory.Models.ViewModels;


namespace Inventory.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        private ISupplierRepository supplierRepository;
        public int PageSize = 4;

        public ProductController(IProductRepository repo, ISupplierRepository supplierRepo)
        {
            repository = repo;
            supplierRepository = supplierRepo;
        }

        //public ViewResult List() => View(repository.Products);
        /*
        public ViewResult List(int productPage = 1)
            => View(repository.Products
                .OrderBy(p => p.ProductID)
                .Skip((productPage - 1) * PageSize)
                .Take(PageSize));
                */
        public ViewResult List(string supplierName, int productPage = 1)
            => View(new ProductsListViewModel
            {
                Products = (supplierName == null) ?
                    repository.Products
                    .OrderBy(p => p.ProductID)
                    .Skip((productPage -1)*PageSize)
                    .Take(PageSize)
                    : repository.Products
                    .Where(p => p.Supplier == null || p.Supplier.Name == supplierName)
                    .OrderBy(p => p.ProductID)
                    .Skip((productPage - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = productPage,
                    ItemsPerPage = PageSize,
                    TotalItems = supplierName == null ?
                        repository.Products.Count() :
                        repository.Products.Where(p => 
                            p.Supplier.Name == supplierName).Count()
                },
                CurrentSupplierName = supplierName
            });

        public IActionResult Index()
        {
            return View(repository);
        }
    }
}