﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Inventory.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Inventory
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration["Data:InventoryProducts:ConnectionString"]));
            services.AddTransient<IProductRepository, EFProductRepository>();
            services.AddTransient<ISupplierRepository, EFSupplierRepository>();
            services.AddTransient<IOrderRepository, EFOrderRepository>();
            services.AddTransient<ICartRepository, EFCartRepository>();
            services.AddMvc();
            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
                app.UseStaticFiles();
                app.UseSession();
                
                app.UseMvc(routes =>
                {
                    /*
                    routes.MapRoute(
                        name: "pagination",
                        template: "Products/Page{productPage}",
                        defaults: new { Controller = "Product", action = "List"});
                    */
                    routes.MapRoute(
                        name: null,
                        template: "{supplierName}/Page{productPage:int}",
                        defaults: new {controller = "Product", action = "List"}
                        );
                    routes.MapRoute(
                        name: null,
                        template: "Page{productPage:int}",
                        defaults: new
                        {
                            controller = "Product",
                            action = "List",
                            productPage = 1
                        }
                        );
                    routes.MapRoute(
                        name: null,
                        template: "{supplierName}",
                        defaults: new {controller = "Product",
                            action="List", productPage=1}
                        );

                    routes.MapRoute(name: null, template: "{controller}/{action}/{id?}");

                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}");
                });
                
                //app.UseMvcWithDefaultRoute();
            }

            SeedData.EnsurePopulated(app);
            
        }
    }
}
