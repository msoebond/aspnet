﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    interface ICartRepository
    {
        IQueryable<Cart> Carts { get; }
    }
}
