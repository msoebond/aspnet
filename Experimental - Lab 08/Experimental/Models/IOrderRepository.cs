﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    interface IOrderRepository
    {
        IQueryable<Order> Orders { get; }
    }
}
