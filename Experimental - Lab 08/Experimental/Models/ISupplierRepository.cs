﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public interface ISupplierRepository
    {
        IQueryable<Supplier> Suppliers { get; }
    }
}
