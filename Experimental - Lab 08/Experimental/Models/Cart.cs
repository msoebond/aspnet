﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Cart
    {
        public int CartId { get; set; }
        private List<CartLine> lineCollection = new List<CartLine>();
        public virtual IEnumerable<CartLine> Lines => lineCollection;

        public virtual void AddItem(Product product, int qtySets)
        {
            CartLine line = lineCollection
                .Where(p => p.Product.ProductID == product.ProductID)
                .FirstOrDefault();

            if(line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Product = product,
                    QtySets = qtySets
                });
            }
            else
            {
                line.QtySets += qtySets;
            }
        }

        public virtual void RemoveLine(Product product)
        {
            lineCollection.RemoveAll(l => l.Product.ProductID == product.ProductID);
        }

        public virtual void Clear() => lineCollection.Clear();
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public Product Product { get; set; }
        public int QtySets { get; set; }
    }
}
