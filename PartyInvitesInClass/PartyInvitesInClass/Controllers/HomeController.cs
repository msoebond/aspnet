﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PartyInvitesInClass.Models;

namespace PartyInvitesInClass.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            int hour = DateTime.Now.Hour;
            string greeting = hour < 12 ? "Good Moring" : "Good Afternoon";
            greeting += " the current time is " + DateTime.Now.ToShortTimeString();

            ViewBag.Greeting = greeting;

            return View();
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse resp)
        {
            if (ModelState.IsValid)
            {
                Repository.AddResponse(resp);
                return View("Thanks", resp);
            }
            else
            {
                return View(resp);
            }
            
            
        }

        public ViewResult ListResponses()
        {
            return View(Repository.Responses.Where(r => r.WillAttend == true));
        }
    }
}