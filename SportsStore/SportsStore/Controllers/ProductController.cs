﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        private int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(int pageNo = 1)
        {

            ProductListViewModel model = new ProductListViewModel();

            IEnumerable<Product> pagedProducts = repository.Products
                .OrderBy(p => p.ProductID)
                .Skip((pageNo -1 ) * PageSize)
                .Take(PageSize);

            model.Products = pagedProducts;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = repository.Products.Count()
            };

            model.PageInfo = pInfo;
            
            return View(model);

           
        }
    }
}