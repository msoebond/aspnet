﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class EFProductRepository : IProductRepository
    {
        private ApplicationDbContext context;

        public EFProductRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }
        
        public IQueryable<Supplier> Suppliers => context.Suppliers;

        public IQueryable<Product> Products => context.Products
            .Include(p => p.Supplier);

        public IQueryable<Order> Orders => context.Orders
            .Include(o => o.Lines)
            .ThenInclude(l => l.Product);

        public IQueryable<Cart> Carts => context.Carts
            .Include(c => c.Lines)
            .ThenInclude(l => l.Product);


    }
}
