﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public interface IProductRepository
    {
        IQueryable<Supplier> Suppliers { get; }
        IQueryable<Product> Products { get; }
        IQueryable<Order> Orders { get; }
        IQueryable<Cart> Carts { get; }
    }
}
