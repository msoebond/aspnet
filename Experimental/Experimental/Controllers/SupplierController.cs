﻿using Inventory.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inventory
{
    public class SupplierController : Controller
    {
        private IProductRepository repository;
        public SupplierController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List() => View(repository.Suppliers);

        public IActionResult Index()
        {
            return View();
        }
    }
}