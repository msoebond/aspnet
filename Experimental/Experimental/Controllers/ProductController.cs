﻿using Inventory.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inventory
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List() => View(repository.Products);

        public IActionResult Index()
        {
            return View(repository);
        }
    }
}