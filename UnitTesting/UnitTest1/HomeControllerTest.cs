﻿using Microsoft.AspNetCore.Mvc;
using Razor.Controllers;
using Razor.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Linq;

namespace UnitTest1
{
    public class HomeControllerTest
    {
        [Fact]
        public void IndexActionIsComplete()
        {
            HomeController controller = new HomeController();

            //actual
            ViewResult result = (ViewResult)controller.Index();
            IEnumerable<Product> modelProducts = (IEnumerable<Product>)result?.ViewData.Model;

            //expected
            IEnumerable<Product> repoProducts = 
                SimpleRepository.SharedRepo.Products
                .Where(p => p.Price < 50)
                .OrderBy(p => p.Name);

            Assert.Equal(repoProducts, modelProducts,
                Comparer.Get<Product>((p1, p2) => 
                p1.Name == p2.Name && p1.Price == p2.Price));
        }
    }
}
