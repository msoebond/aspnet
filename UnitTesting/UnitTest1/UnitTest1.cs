using System;
using Xunit;
using Razor.Models;

namespace UnitTest1
{
    public class UnitTest1
    {
        [Fact]
        public void CanChangeProductName()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };

            //Act
            p.Name = "New Name";

            //Assert
            Assert.Equal("New Name", p.Name);

        }

        [Fact]
        public void CanChangeProductPrice()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };

            //Act
            p.Price = 200m;

            //Assert
            Assert.Equal(200m, p.Price);

        }
    }
}
