﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Razor.Models;

namespace Razor.Controllers
{
    public class HomeController : Controller
    {
        SimpleRepository repo = SimpleRepository.SharedRepo;

        public ViewResult Index()
        {
            IEnumerable<Product> fRepo =
               repo.Products
               .Where(p => p.Price < 50)
               .OrderBy(p=>p.Name);

            return View(fRepo);
        }

        [HttpGet]
        public IActionResult AddProduct()
        {
            return View(new Product());
        }

        [HttpPost]
        public IActionResult AddProduct(Product p)
        {
            repo.AddProduct(p);
            return RedirectToAction("Index");
        }
    }
}