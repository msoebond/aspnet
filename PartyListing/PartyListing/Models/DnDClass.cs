﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PartyListing.Models
{
    public enum Classes
    {
        Fighter,
        Ranger,
        Wizard,
        Cleric,
        Rogue
    }
    public class DnDClass
    {
        public string Name { get; set; }
        public Classes Class { get; set; }
        public int Experience { get; set; }

        public int Level
        {
            get
            {
                return Experience / 1000;
            }
            set
            {
                Experience = value * 1000;
            }
        }

    }
}
