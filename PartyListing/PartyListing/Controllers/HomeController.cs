﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PartyListing.Models;

namespace PartyListing.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            DnDClass[] arr =
            {
                new DnDClass { Name = "Bjorn", Class = Classes.Fighter, Experience = 3300 },
                new DnDClass { Name = "Alyx", Class = Classes.Wizard, Experience = 2900 }
            };
            return View(arr);
        }
    }
}