﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class EFSupplierRepository : ISupplierRepository
    {
        private ApplicationDbContext context;

        public EFSupplierRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Supplier> Suppliers => context.Suppliers;
    }
}
