﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public static class OrderSplitter
    { 

        public static IEnumerable<Order> DivideOrders(Cart cart)
        {
            List<Order> orders = new List<Order>();

            foreach(CartLine cl in cart.Lines){
                Supplier s = cl.Product.Supplier;
                Order or = orders.Where(o => o.Supplier == s).FirstOrDefault();
                if (or == null)
                {
                    or = new Order(s);
                }

                or.Add(new OrderLine { Product = cl.Product, QtySets = cl.QtySets });

            }
            return orders;
        }
    }
}
