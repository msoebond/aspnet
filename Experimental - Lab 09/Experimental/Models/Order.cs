﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public Supplier Supplier { get; }
        private List<OrderLine> lineCollection = new List<OrderLine>();
        public IEnumerable<OrderLine> Lines => lineCollection;


        public Order(Supplier sup)
        {
            this.Supplier = sup;
        }

        public void Add(OrderLine ol)
        {
            lineCollection.Add(ol);
        }
    }

    public class OrderLine
    {
        public int OrderLineID { get; set; }
        public Product Product { get; set; }
        public int QtySets { get; set; }
    }

}
