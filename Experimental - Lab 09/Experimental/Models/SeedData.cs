﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Inventory.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Products.Any())
            {
                Supplier pepsi = new Supplier { Name = "Pepsi of La Crosse" };
                Supplier kemps = new Supplier { Name = "Kemps" };
                context.Suppliers.AddRange(
                    pepsi,
                    kemps
                    );
                context.Products.AddRange(
                    new Product
                    {
                        Name = "Pepsi 20oz Bottle",
                        currentNo = 100,
                        unitsInSet = 24,
                        orderAtNo = 50,
                        defaultOrderNo = 4,
                        Supplier = pepsi
                    },
                    new Product
                    {
                        Name = "Diet Pepsi 20oz Bottle",
                        currentNo = 97,
                        unitsInSet = 24,
                        orderAtNo = 40,
                        defaultOrderNo = 3,
                        Supplier = pepsi
                    },
                    new Product
                    {
                    Name = "Mountain Dew 20oz Bottle",
                        currentNo = 145,
                        unitsInSet = 24,
                        orderAtNo = 70,
                        defaultOrderNo = 5,
                        Supplier = pepsi
                    },
                    new Product
                    {
                    Name = "Kemps 1gal Whole Milk",
                        currentNo = 30,
                        unitsInSet = 4,
                        orderAtNo = 24,
                        defaultOrderNo = 6,
                        Supplier = kemps
                    },
                    new Product
                    {
                        Name = "Kemps 1/2gal Whole Milk",
                        currentNo = 2,
                        unitsInSet = 9,
                        orderAtNo = 5,
                        defaultOrderNo = 1,
                        Supplier = kemps
                    },
                    new Product
                    {
                        Name = "Kemps 1gal Chocolate Milk",
                        currentNo = 12,
                        unitsInSet = 4,
                        orderAtNo = 3,
                        defaultOrderNo = 3,
                        Supplier = kemps
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}
