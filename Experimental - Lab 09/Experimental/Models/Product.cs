﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public int unitsInSet { get; set; } // e.g. if you have to order a dozen at a time because they come in a case
        public int currentNo { get; set; }
        public int orderAtNo { get; set; } // i.e. when there is this number or fewer on hand, more should be ordered
        public int defaultOrderNo { get; set; } // just makes reordering a little easier so the the number of sets does not have to manually entered on an order - but could be
        public int SupplierID { get; set; }

        public Supplier Supplier { get; set; }
    }
}
