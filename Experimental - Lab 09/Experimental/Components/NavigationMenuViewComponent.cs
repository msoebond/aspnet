﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Models;

namespace Inventory.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private ISupplierRepository repository;

        public NavigationMenuViewComponent(ISupplierRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedSupplier = RouteData?.Values["supplierName"];
            return View(repository.Suppliers
                .Select(s=>s.Name));
        }
    }
}
