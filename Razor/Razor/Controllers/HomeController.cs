﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Razor.Models;

namespace Razor.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            /*
            Product p = new Product()
            {
                ProductId = 1,
                Name = "Kayak",
                Description = "Singlenperson boat",
                Category = "Watersport",
                Price = 275m
            };

            ViewBag.StockLevel = 2;
            */

            Product[] arr = new Product[]
            {
                new Product{Name = "Kayak", Price=275m},
                new Product{Name = "Canoe", Price=800m},
                new Product{Name = "Paddle", Price=15m}
            };

            return View(arr);
        }
    }
}