﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class EFAuthorRepository : IAuthorRepository
    {
        private BookDbContext context;
        public IEnumerable<Author> Authors => context.Authors;

        public EFAuthorRepository(BookDbContext context)
        {
            this.context = context;
        }
    }
}
