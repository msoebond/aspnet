﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Models
{
    public static class MyExtensionMethods
    {
        /*
        public static decimal TotalPrices(this ShoppingCart cartParam)
        {
            decimal total = 0m;
            foreach(Product p in cartParam.Products)
            {
                total += p?.Price ?? 0m;
            }

            return total;
        }
        */

        public static decimal TotalPrices(this IEnumerable<Product> products)
        {
            decimal total = 0m;
            foreach (Product p in products)
            {
                total += p?.Price ?? 0m;
            }

            return total;
        }


        public static IEnumerable<Product> FilterByPrice(this IEnumerable<Product> products, decimal minPrice)
        {
            //List<Product> filProducts = new List<Product>();
            foreach(Product p in products)
            {
                if (p?.Price > minPrice)
                {
                    //filProducts.Add(p);
                    yield return p;
                }
            }
            //return filProducts;
        }

        public static IEnumerable<Product> FilterByName(this IEnumerable<Product> products, char firstChar)
        {
            foreach (Product p in products)
            {
                if (p?.Name?[0] == firstChar)
                {
                    yield return p;
                }
            }
        }

        public static IEnumerable<Product> Filter(
            this IEnumerable<Product> products, 
            Func<Product, bool> selector)
        {
            foreach (Product p in products)
            {
                if (selector(p))
                {
                    yield return p;
                }
            }
        }
    }
}
