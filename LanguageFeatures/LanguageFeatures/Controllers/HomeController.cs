﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;


namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {

        bool FilterByPrice(Product p)
        {
            return (p?.Price ?? 0) >= 50;
        }

        /*
        public ViewResult Index()
        {
            return View(new string[] { "C#", "Language", "Features" });
        }
        */

        //public ViewResult Index()
        //{
        //    List<string> results = new List<string>();
        //    foreach(Product p in Product.GetProducts())
        //    {
        //        string name = p?.Name ?? "<No Name>";
        //        decimal? price = p?.Price ?? 0.0m;
        //        /*
        //        results.Add(

        //            string.Format("Name: {0}, Price: {1} Related: {2}"
        //            , name, price, p?.Related?.Name ?? "<No Related>"));
        //        */
        //        results.Add($"Name: {name}, Price: {price:C2}, Related: {p?.Related?.Name ?? "<No Relation>"}");

        //    }

        //    return View(results);

        //}
        //public ViewResult Index()
        //{
        //    /*
        //    Dictionary<string, Product> products =
        //        new Dictionary<string, Product>() {
        //            { "Kayak", new Product(){Name = "Kayak", Price = 275m} },
        //            {"Lifejacket", new Product{Name="Lifejacket", Price = 48.95m} }
        //        };
        //        */
        //    Dictionary<string, Product> products =
        //        new Dictionary<string, Product>() {
        //            ["Kayak"] = new Product { Name = "Kayak", Price = 275m },
        //            ["Lifejacket"] = new Product { Name="Lifejacket", Price = 48.95m }
        //        };
        //    return View(products.Keys);
        //}

        //public ViewResult Index()
        //{
        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProducts()
        //    };

        //    Product[] prodArr =
        //    {
        //        new Product{Name = "Canoe", Price=2000},
        //        new Product{Name="Paddle", Price=15}
        //    };

        //    decimal totalPrice = cart.TotalPrices();

        //    return View("Index", 
        //        new string[] {
        //            $"Old Total: {totalPrice}",
        //            $"New Total: {prodArr.TotalPrices()}"
        //        });
        //}

        //public ViewResult Index()
        //{
        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProducts()
        //    };
        //    //decimal expensiveTotal = cart.FilterByPrice(50).TotalPrices();
        //    decimal expensiveTotal = cart.Filter(FilterByPrice).TotalPrices();
        //    //decimal nameTotal = cart.FilterByName('K').TotalPrices();

        //    Func<Product, bool> nameFilter = delegate (Product p)
        //    {
        //        return p?.Name?[0] == 'S';
        //    };

        //    return View("Index",
        //        new string[] {
        //                $"Old Total: {cart.TotalPrices()}",
        //                $"Exp Total: {expensiveTotal}"
        //        });
        //}

        //public ViewResult Index()
        //{
        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProducts()
        //    };
        //    IEnumerable<Product> fProducts = cart.Filter(p => (p?.Price ?? 0) > 50);
        //    decimal priceTotal = fProducts.TotalPrices();

        //    decimal nameTotal = cart.Filter(p => p?.Name[0] == 'K').TotalPrices();

        //    return View("Index",
        //        new string[] {
        //                    $"Old Total: {cart.TotalPrices()}",
        //                    $"Exp Total: {priceTotal}",
        //                    $"Name Total {nameTotal}"
        //        });
        //}

        public async Task<ViewResult> Index()
        {
            //return View(Product.GetProducts().Select(p => p?.Name));
            long? length = await AsyncMethods.GetPageLength();
            return View(new string[] { $"Length : {length}" });
        }
    }
}