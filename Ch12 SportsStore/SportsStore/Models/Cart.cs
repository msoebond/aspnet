﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();
        public IEnumerable<CartLine> Lines => lineCollection;

        public virtual void AddItem(Product prod, int qty) {
            CartLine line = lineCollection
            //.Where(l => l.Product.ProductId == Product.ProductId)
            //.FirstOrDefault();
            .FirstOrDefault(l => l.Product.ProductID == prod.ProductID);

            if(line == null)
            {
                lineCollection.Add(
                    new CartLine()
                    {
                        Product = prod,
                        Quantity = qty
                    });
            }
            else
            {
                line.Quantity += qty;
            }
        }

        // Clear the cart method
        public virtual void Clear() => lineCollection.Clear();

        public virtual void RemoveLine(Product prod)
        {
            lineCollection
                .RemoveAll(l => l.Product.ProductID == prod.ProductID);
        }

        public virtual decimal ComputeTotalValue()
        {
            return lineCollection
                .Sum(e => e.Product.Price * e.Quantity);
        }
        

    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
