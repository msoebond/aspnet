﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;

namespace SportsStore.Controllers
{
    
    public class AdminController : Controller
    {
        private IProductRepository productRepo;

        public AdminController(IProductRepository repo)
        {
            productRepo = repo;
        }
        public IActionResult Index()
        {
            return View(productRepo.Products);
        }

        public ViewResult Edit(int productId)
        {
            if(productId == 0)
            {
                return View(new Product());
            }
            return View(productRepo.Products
                .FirstOrDefault(p => p.ProductID == productId));
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                //save
                productRepo.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                //something wrong with data
                return View(product);
            }
        }

        public ActionResult Create()
        {
            //return RedirectToAction("Edit", new { productId = 0 });//takes an object so need a new anonymous object
            return View("Edit", new Product());
        }

        [HttpPost]
        public IActionResult Delete(int productId)
        {
            /*
            Product product = productRepo.Products.FirstOrDefault(p => p.ProductID == productId);
            if(product != null)
            {
                productRepo.DeleteProduct(product);
                TempData["message"] = $"{product.Name} has been deleted";
            }
            else
            {
                TempData["message"] = $"{product.Name} was not found";
            }
            */

            Product product = productRepo.DeleteProduct(productId);
            
            if(product != null)
            {
                TempData["message"] = $"{product.Name} has been deleted";
            }
            else
            {
                TempData["message"] = $"{product.Name} was not found";
            }

                
            



            return View("Index", productRepo.Products);
        }
    }
}