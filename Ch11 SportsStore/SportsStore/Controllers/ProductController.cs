﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        private int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(string category, int pageNo = 1)
        {

            ProductListViewModel model = new ProductListViewModel();
            IEnumerable<Product> pagedProducts;
            if (category == null)
            {
                pagedProducts = repository.Products
                    .OrderBy(p => p.ProductID)
                    .Skip((pageNo - 1) * PageSize)
                    .Take(PageSize);
            }
            else
            {
                pagedProducts = repository.Products
                    .Where(p => p.Category == null || p.Category == category)
                    .OrderBy(p => p.ProductID)
                    .Skip((pageNo -1 ) * PageSize)
                    .Take(PageSize);
            }
            

            model.Products = pagedProducts;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = category == null ?
                    repository.Products
                    .Count() :
                    repository.Products
                    .Where(p=> p.Category == category)
                    .Count()
            };

            model.CurrentCategory = category;

            model.PageInfo = pInfo;
            
            return View(model);

           
        }
    }
}