﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class EFProductRepository : IProductRepository
    {
        private ApplicationDbContext context;
        public EFProductRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Product> Products => context.Products;

        public void DeleteProduct(Product product)
        {
            Product prod = context.Products
                .FirstOrDefault(p => p.ProductID == product.ProductID);

            if(prod != null)
            {
                context.Products.Remove(prod);
            }

            context.SaveChanges();
        }

        public Product DeleteProduct(int productId)
        {
            Product prod = context.Products
                .FirstOrDefault(p => p.ProductID == productId);

            if (prod != null)
            {
                context.Products.Remove(prod);
            }

            context.SaveChanges();

            return prod;
        }

        public void SaveProduct(Product product)
        {
            if(product.ProductID == 0)
            {
                //add new product
                context.Products.Add(product);   
            }
            else
            {
                //update product
                Product efProduct = context.Products
                    .FirstOrDefault(p => p.ProductID == product.ProductID);
                if(efProduct != null)
                {
                    efProduct.Name = product.Name;
                    efProduct.Category = product.Category;
                    efProduct.Description = product.Description;
                    efProduct.Price = product.Price;
                }
            }
            context.SaveChanges();
        }
    }
}
