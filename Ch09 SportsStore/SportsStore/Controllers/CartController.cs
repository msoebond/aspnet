﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Infrastructure;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {
        private IProductRepository prodRepo;

        public CartController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public IActionResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel{
                Cart = GetCart(),
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public RedirectToActionResult AddToCart(int productID,
            string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productID);
            if(prod != null)
            {
                Cart cart = GetCart();
                cart.AddItem(prod, 1);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromCart(int productID,
    string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductID == productID);
            if (prod != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(prod);
                SaveCart(cart);
            }

            return RedirectToAction("Index", new { returnURL });
        }

        private Cart GetCart()
        {
            
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }

        
    }
}
