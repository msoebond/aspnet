﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDTracker.Model
{
    public class Portfolio
    {
        private static List<CD> cds = new List<CD>();

        public static List<CD> portfolio
        {
            get
            {
                return cds;
            }
        }

        public static void AddCD(CD cd)
        {
            cds.Add(cd);
        }

        /**
         * So that I don't have to return the whole list to see if it is empty
         */
        public static bool isEmpty()
        {
            return cds.Count <= 0; // Use '<=' just to cover eventualities
           
        }
    }
}
