﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDTracker.Model
{
    public class CD
    {
        public String BankName { get; set; }
        public int Term { get; set; } //months
        public double Rate { get; set; } //annual
        public DateTime PurchaseDate { get; set; }
        public decimal Deposit { get; set; } // the initial deposit amount in dollars

        public DateTime MaturityDate
        {
            get{return calcMaturityDate();}
        }
        public decimal ValueAtMaturity
        {
            get{return calcValueAtMaturity();}
        }

        // *** METHODS ***
        private DateTime calcMaturityDate()
        {
            DateTime returnDT = new DateTime(PurchaseDate.Year, PurchaseDate.Month, PurchaseDate.Day);
            return returnDT.AddMonths(Term);
        }

        private decimal calcValueAtMaturity()
        {
            // compounded monthly
            //decimal valueMaturity = Deposit*(decimal)(Math.Pow(1.0+(Rate/12.0),Rate)); // Note: it is just Rate in the exponent because the 12s cancel on monthly compounding
            decimal valueMaturity = Deposit * (decimal)Math.Exp(Rate/100.0 * Term / 12); //Continuously compounded
            return Decimal.Round(valueMaturity,2);
        }



    }
}
