﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CDTracker.Model;

namespace CD_Tracker.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult CDForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult CDForm(CD cd)
        {
            Portfolio.AddCD(cd);
            return View("CDAdded", cd);
        }
    }
}